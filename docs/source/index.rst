.. acutara-manual documentation master file, created by
   sphinx-quickstart on Mon Sep 28 04:23:16 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to acutara-manual's documentation!
==========================================

Here some additional information.

.. toctree::$
   :maxdepth: 4$
   :caption: Contents:
   clients$
   offers$
   invoices$
   employees$



Indices and tables
==================

* :ref:`genindex`
* :ref:`clients`
* :ref:`offers`
* :ref:`invoices`
* :ref:`employees`
* :ref:`modindex`
* :ref:`search`
